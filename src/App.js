import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import { clvDevice } from 'clv-sipjs';

function App() {
  const [_device, setDevice] = useState(null);
  const [Incoming, setIncoming] = useState(null);
  const [activeCall, setActiveCall] = useState(null);
  const [phone, setPhone] = useState('');
  useEffect(()=>{
    let config = {
      host: '',
      port: '4433',
      displayName: '106',
      username: '106',
      password: '',
      wsServers: 'wss://:4433',
    };
    
    let device = new clvDevice(config);
    setDevice(device);
    
    device.on('connecting', () => {
      console.log("connecting!");
    });
    device.on('accepted', () => {
      console.log("We're on a phone call!");
    });
    device.on('invite', (data) => {
      setIncoming(data)
      console.log("Incoming", data)
    });
    device.on('accept', (accept) => {
      console.log("accept", accept)
    })
    device.on('cancel', (cancel) => {
      console.log("cancel", cancel)
    })
    device.on('rejected', (rejected) => {
      console.log("rejected", rejected)
    })
    device.on('failed', (failed) => {
      console.log("failed", failed)
    })
    device.on('bye', (bye) => {
      setIncoming('')
      console.log("bye", bye)
    })
    device.on('replaced', (replaced) => {
      console.log("replaced", replaced)
    })
    device.on('rejected', (rejected) => {
      console.log("rejected", rejected)
    })
    device.on('trackAdded', (trackAdded) => {
      console.log("trackAdded", trackAdded)
    })
    console.log("device", device)
  },[])
  const handlecall = () =>{
    let activeCall = _device.initiateCall(phone);
    setActiveCall(activeCall)
    activeCall.on('connecting', () => {
      console.log("it's connecting!");
    });
    activeCall.on('accepted', () => {
      console.log("We're on a phone call!");
    });
  }  

  const handleAccept = () =>{
    _device.accept();
  }
  return (
    <div className="App">
      <audio id="clv-sipjs-remote-audio"></audio>
      <header className="App-header">
        {
          Incoming ? <><p>Incoming: {Incoming.phoneNumber}</p>  <a
          className="App-link"
         onClick={handleAccept}
        >
         Accept
        </a></>: null
        }
        <div>
        <input onChange={(e) => setPhone(e.target.value)} />
        <a
          className="App-link"
         onClick={handlecall}
        >
         Call
        </a>
        </div>
      </header>
    </div>
  );
}

export default App;
